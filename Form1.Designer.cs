﻿namespace MCtty
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rtb_Display = new System.Windows.Forms.RichTextBox();
            this.SerialComPort = new System.IO.Ports.SerialPort(this.components);
            this.comboBox_COM = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Button_Conn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_SPEED = new System.Windows.Forms.TextBox();
            this.comboBox_PARITY = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxDATABIT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_STOP = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_HANDSHAKE = new System.Windows.Forms.ComboBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtb_Display
            // 
            this.rtb_Display.BackColor = System.Drawing.SystemColors.WindowText;
            this.rtb_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb_Display.ForeColor = System.Drawing.SystemColors.Menu;
            this.rtb_Display.Location = new System.Drawing.Point(12, 137);
            this.rtb_Display.Name = "rtb_Display";
            this.rtb_Display.Size = new System.Drawing.Size(780, 303);
            this.rtb_Display.TabIndex = 0;
            this.rtb_Display.Text = "";
            // 
            // SerialComPort
            // 
            this.SerialComPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.SerialComPort_DataReceived);
            // 
            // comboBox_COM
            // 
            this.comboBox_COM.FormattingEnabled = true;
            this.comboBox_COM.Location = new System.Drawing.Point(12, 28);
            this.comboBox_COM.Name = "comboBox_COM";
            this.comboBox_COM.Size = new System.Drawing.Size(128, 21);
            this.comboBox_COM.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Porte COM disponibili";
            // 
            // Button_Conn
            // 
            this.Button_Conn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Conn.Location = new System.Drawing.Point(15, 88);
            this.Button_Conn.Name = "Button_Conn";
            this.Button_Conn.Size = new System.Drawing.Size(113, 22);
            this.Button_Conn.TabIndex = 3;
            this.Button_Conn.Text = "Connect COM";
            this.Button_Conn.UseVisualStyleBackColor = true;
            this.Button_Conn.Click += new System.EventHandler(this.Button_Conn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(192, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "SPEED";
            // 
            // textBox_SPEED
            // 
            this.textBox_SPEED.Location = new System.Drawing.Point(195, 29);
            this.textBox_SPEED.Name = "textBox_SPEED";
            this.textBox_SPEED.Size = new System.Drawing.Size(100, 20);
            this.textBox_SPEED.TabIndex = 6;
            // 
            // comboBox_PARITY
            // 
            this.comboBox_PARITY.FormattingEnabled = true;
            this.comboBox_PARITY.Location = new System.Drawing.Point(504, 28);
            this.comboBox_PARITY.Name = "comboBox_PARITY";
            this.comboBox_PARITY.Size = new System.Drawing.Size(79, 21);
            this.comboBox_PARITY.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(501, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "PARITY";
            // 
            // textBoxDATABIT
            // 
            this.textBoxDATABIT.Location = new System.Drawing.Point(321, 29);
            this.textBoxDATABIT.Name = "textBoxDATABIT";
            this.textBoxDATABIT.Size = new System.Drawing.Size(71, 20);
            this.textBoxDATABIT.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(318, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "DATA BIT";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(406, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "STOP";
            // 
            // comboBox_STOP
            // 
            this.comboBox_STOP.FormattingEnabled = true;
            this.comboBox_STOP.Location = new System.Drawing.Point(409, 28);
            this.comboBox_STOP.Name = "comboBox_STOP";
            this.comboBox_STOP.Size = new System.Drawing.Size(71, 21);
            this.comboBox_STOP.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(604, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "HANDSHAKE";
            // 
            // comboBox_HANDSHAKE
            // 
            this.comboBox_HANDSHAKE.FormattingEnabled = true;
            this.comboBox_HANDSHAKE.Location = new System.Drawing.Point(607, 28);
            this.comboBox_HANDSHAKE.Name = "comboBox_HANDSHAKE";
            this.comboBox_HANDSHAKE.Size = new System.Drawing.Size(132, 21);
            this.comboBox_HANDSHAKE.TabIndex = 13;
            // 
            // buttonClear
            // 
            this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.Location = new System.Drawing.Point(717, 109);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(71, 22);
            this.buttonClear.TabIndex = 15;
            this.buttonClear.Text = "Pulisci";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox_HANDSHAKE);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox_STOP);
            this.Controls.Add(this.textBoxDATABIT);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox_PARITY);
            this.Controls.Add(this.textBox_SPEED);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Button_Conn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_COM);
            this.Controls.Add(this.rtb_Display);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SERIALE";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_Display;
        private System.IO.Ports.SerialPort SerialComPort;
        private System.Windows.Forms.ComboBox comboBox_COM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Button_Conn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_SPEED;
        private System.Windows.Forms.ComboBox comboBox_PARITY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxDATABIT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_STOP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_HANDSHAKE;
        private System.Windows.Forms.Button buttonClear;
    }
}

