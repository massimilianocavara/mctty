﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace MCtty
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FInitSeriale();
        }

        /********************************************************************
        * inizializzazione della seriale
        * 
        *
        ********************************************************************/
        public void FInitSeriale()
        {
            //Port setting for Com Port Serial comunication
            //SerialComPort.BaudRate = 57600;
            //SerialComPort.DataBits = 8;
            //SerialComPort.DiscardNull = false; //absolutely false
            //SerialComPort.DtrEnable = false; //absolutely false
            //SerialComPort.Handshake = System.IO.Ports.Handshake.None;
            //SerialComPort.Parity = System.IO.Ports.Parity.None;
            //SerialComPort.ReadBufferSize = 4096;
            //SerialComPort.ReadTimeout = -1;
            //SerialComPort.ReceivedBytesThreshold = 1;
            //SerialComPort.RtsEnable = false;
            //SerialComPort.StopBits = System.IO.Ports.StopBits.One;
            //SerialComPort.WriteBufferSize = 4096;
            //SerialComPort.WriteTimeout = 5;

            //popolo la combobox e textbox con default e altre possibilità

            foreach (string s in SerialPort.GetPortNames())
            {
                comboBox_COM.Items.Add(s.Trim());
            }

            textBox_SPEED.Text = SerialComPort.BaudRate.ToString();
            textBoxDATABIT.Text = SerialComPort.DataBits.ToString();

            foreach (string s in Enum.GetNames(typeof(Parity)))
            {
                comboBox_PARITY.Items.Add(s);
            }

            foreach (string s in Enum.GetNames(typeof(StopBits)))
            {
                comboBox_STOP.Items.Add(s);
            }

            foreach (string s in Enum.GetNames(typeof(Handshake)))
            {
                comboBox_HANDSHAKE.Items.Add(s);
            }
            comboBox_COM.SelectedItem = null;
            comboBox_COM.Text = "...select...";
            comboBox_STOP.SelectedItem = null;
            comboBox_STOP.Text = "...select...";
            comboBox_PARITY.SelectedItem = null;
            comboBox_PARITY.Text = "...select...";
            comboBox_HANDSHAKE.SelectedItem = null;
            comboBox_HANDSHAKE.Text = "...select...";
        }

        /********************************************************************
        * apertura della seriale
        * 
        *
        ********************************************************************/
        private void OpenSerialPort()
        {
            //Try to connect to Serial Port  

            if (SerialComPort.IsOpen)
            {
                SerialComPort.Close();
            }

            try
            {
                //Get com port from combo box
                SerialComPort.PortName = comboBox_COM.SelectedItem.ToString();
                SerialComPort.BaudRate = int.Parse(textBox_SPEED.Text);
                SerialComPort.DataBits = int.Parse(textBoxDATABIT.Text.ToUpperInvariant());
                //SerialComPort.StopBits = (StopBits)comboBox_STOP.SelectedItem;
                //SerialComPort.Parity = (Parity)comboBox_PARITY.SelectedItem;
                //SerialComPort.Handshake = (Handshake)comboBox_HANDSHAKE.SelectedItem;
                SerialComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), comboBox_STOP.SelectedItem.ToString(), true);
                SerialComPort.Parity = (Parity)Enum.Parse(typeof(Parity), comboBox_PARITY.SelectedItem.ToString(), true);
                SerialComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), comboBox_HANDSHAKE.SelectedItem.ToString(), true);


                //open serial port
                SerialComPort.Open();

                //green combo box on opened port
                comboBox_COM.BackColor = Color.GreenYellow;

                //Flush and discard buffer
                SerialComPort.BaseStream.Flush();
                SerialComPort.DiscardInBuffer();
                SerialComPort.DiscardOutBuffer();
                rtb_Display.AppendText("# SERIAL connection opened, COM port: " + comboBox_COM.Text + "\n");
            }
            catch (Exception ex)
            {
                //Red combo box on error
                comboBox_COM.BackColor = Color.Red;

                //MessageBox Com Error
                MessageBox.Show(ex.Message, "I can't open serial port !!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                rtb_Display.AppendText("# SERIAL connection error: " + ex.Message + "\n");
            }
        }

        /********************************************************************
         * chiusura della seriale
         * 
         *
         ********************************************************************/
        private void CloseSerialPort()
        {

            //Try to connect to Serial Port      
            if (SerialComPort.IsOpen)
            {
                SerialComPort.Close();
            }

            comboBox_COM.BackColor = Color.Red;
            rtb_Display.AppendText("# SERIAL connection closed, COM port: " + comboBox_COM.Text + "\n");
        }

        private void Button_Conn_Click(object sender, EventArgs e)
        {
            if (SerialComPort.IsOpen)
            {
                CloseSerialPort();
                Button_Conn.Text = "Connect COM";
            }
            else
            {
                if ((comboBox_COM.SelectedItem != null) & (comboBox_STOP.SelectedItem != null) & (comboBox_PARITY.SelectedItem != null) & (comboBox_HANDSHAKE.SelectedItem != null)) {
                    OpenSerialPort();
                    Button_Conn.Text = "Disconnect COM";
                } else
                {
                    rtb_Display.AppendText("selezionare tutti i parametri!\n");
                }
                
            }
        }

        /********************************************************************
        * 
        * handler di ricezione da seriale
        *
        ********************************************************************/
        private void SerialComPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] inData = new byte[50]; //abbondo un po'

            System.Threading.Thread.Sleep(100);
            SerialPort sp = (SerialPort)sender;

            int bytesToRead = sp.BytesToRead;
            sp.Read(inData, 0, bytesToRead);
            String Hex_string = BitConverter.ToString(inData, 0, bytesToRead);

            //scrive sul textBox di logging, tramite delegate per evitare conflitto tra Thread
            StampaDatiRx(Hex_string);
        }

        //----------------------------------------------------------------------------------------------
        private void Comm_Write_Log(string text)
        {
            rtb_Display.AppendText(text);
        }

        //----------------------------------------------------------------------------------------------
        private void StampaDatiRx(string dati)
        {
            string Log_complete = "";   //define log string

            Log_complete = Log_complete + "RX: (EXT->PC): " + dati + "\n";

            //Write Log on form and on external txt file
            BeginInvoke(new WriteGUI_Delegate(Comm_Write_Log), new Object[] { Log_complete });
        }
        //**********************************************************************************************
        // Scrittura su form tramite meccanismo dei delegate (puntatore di funzione per
        // evitare accesso da parte di un thread diverso da quello
        // che ha creato il form stesso. 
        // StampaDatiRx invoca il delegate passando la funzione e Log_complete come parametro; il delegato che appartiene
        // al thread che controlla il form, va a scrivere in rtb_Display
        //**********************************************************************************************
        private delegate void WriteGUI_Delegate(string text);

        private void buttonClear_Click(object sender, EventArgs e)
        {
            rtb_Display.Text = "";
        }
    }
}
